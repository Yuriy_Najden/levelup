<section class="business-unique-playground">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">A unique platform for business networking</h2>
                <h3>For people driven by the idea of leading-edge transformations and win-win arrangements between the
                    representatives of business, state, and society
                </h3>
                <div class="items-wrapper">
                    <p class="item"><span class="number">01</span><span class="text">Unity</span></p>
                    <p class="item"><span class="number">02</span><span class="text">Collaboration</span></p>
                    <p class="item"><span class="number">03</span><span class="text">Mutual assistanceч</span></p>
                    <p class="item"><span class="number">04</span><span class="text">Trust</span></p>
                    <p class="item"><span class="number">05</span><span class="text">The theory of six handshakes</span></p>
                    <p class="item"><span class="number">06</span><span class="text">Long-term relations</span></p>
                    <p class="item"><span class="number">07</span><span class="text">Mutually beneficial cooperation</span></p>
                </div>
                <h3>The Level Up Ukraine event yielded over 1,000 valuable contacts and became the place of hundreds of
                    unique acquaintances, profitable deals, and successful investments
                </h3>
                <a href="#participation-packages" class="button scroll-to">Book a ticket</a>
                <img src="img/playgarden-bg-pic.jpg" alt="" class="pic">
            </div>
        </div>
    </div>
</section>