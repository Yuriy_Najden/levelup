<section class="sponsors">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Venue</h2>
                <div class="items-wrapper">
                    <a href="#">
                        <img src="pic/logos/logo1.svg" alt="">
                    </a>

                </div>
                <h2 class="block-title">Become a partner</h2>
                <div class="items-wrapper">
                    <a href="#">
                        <img src="pic/logos/logo11.svg" alt="">
                    </a>
                    <a href="#">
                        <img src="pic/logos/logo12.svg" alt="">
                    </a>
                </div>
                <h2 class="block-title">Sponsors</h2>
                <div class="items-wrapper">
                    <a href="http://bcpp.com.ua/" target="_blank">
                        <img src="pic/logos/logo2.svg" alt="">
                    </a>
                    <a href="https://farmak.ua/" target="_blank">
                        <img src="pic/logos/logo3.svg" alt="">
                    </a>
                    <a href="https://indar.com.ua/ua" target="_blank">
                        <img src="pic/logos/logo4.svg" alt="">
                    </a>
                </div>
                <h2 class="block-title">Media partners</h2>
                <div class="items-wrapper">
                    <a href="http://bcpp.com.ua/" target="_blank">
                        <img src="pic/logos/logo2.svg" alt="">
                    </a>
                    <a href="https://farmak.ua/" target="_blank">
                        <img src="pic/logos/logo3.svg" alt="">
                    </a>
                    <a href="https://indar.com.ua/ua" target="_blank">
                        <img src="pic/logos/logo4.svg" alt="">
                    </a>
                </div>
                <div class="buttons-wrapper" id="SP-type">
                    <a href="#" class="button" data-fname="Partner Proposal" data-teg="mail" data-toggle="modal" data-target="#SP-modal-2">Become a partner</a>
                    <a href="#" class="button" data-fname="Stati Sponsor" data-toggle="modal" data-target="#SP-modal">Become a sponsor</a>
                </div>
            </div>
        </div>
    </div>
</section>