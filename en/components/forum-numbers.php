<section class="forum-numbers">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">This year at the forum Level Up Ukraine 2018</h2>
                <div class="items-wrapper">
                    <div class="item">
                        <p class="border-text">25</p>
                        <p class="text">speakers with world-wide experience</p>
                    </div>
                    <div class="item">
                        <p class="border-text">150</p>
                        <p class="text">owners of various business enterprises</p>
                    </div>
                    <div class="item">
                        <p class="border-text">15</p>
                        <p class="text">renowned international investors</p>
                    </div>
                    <div class="item">
                        <p class="border-text">3</p>
                        <p class="text">speaker sessions and panel discussions</p>
                    </div>
                    <div class="item">
                        <p class="border-text">16</p>
                        <p class="text">master classes and subject-oriented discussions</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>