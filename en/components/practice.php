<section class="practice">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Practical master classes from Level UP Ukraine</h2>
                <h3>The master classes will be given by transformation experts actively involved in the implementation
                    of quality changes at business and corporate levels
                </h3>
                <ul>
                    <li>
                        <p class="number">01</p>
                        <p class="text">Leaders who transform the State and the Nation</p>
                    </li>
                    <li>
                        <p class="number">02</p>
                        <p class="text">Businessmen who transform the industry and raise the quality standards of service
                            delivery
                        </p>
                    </li>
                    <li>
                        <p class="number">03</p>
                        <p class="text">Lawmakers and politicians, who transform the trends and rules of the game</p>
                    </li>
                    <li>
                        <p class="number">04</p>
                        <p class="text">Jurists who transform the approaches and practices</p>
                    </li>
                </ul>
                <h3>Because we are of the belief that discussion without a practice is a blindfolded journey</h3>
                <a href="#participation-packages" class="button scroll-to">Book a ticket</a>
                <img src="img/practic-bg-pic.png" alt="" class="pic">
            </div>

        </div>
    </div>
</section>