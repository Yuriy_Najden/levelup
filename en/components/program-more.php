<section class="program-more collapse" id="program-more">
    <div class="content">
        <div class="parts-wrapper">
            <div class="part part1">
                <h4>Time</h4>
            </div>
            <div class="part part2">
                <h4>Transformation Sessions: Industries </h4>
            </div>
            <div class="part part3">
                <h4>Main Stage</h4>
            </div>
            <div class="part part4">
                <h4>Transformation Sessions: Functions </h4>
            </div>
        </div>
        <h3 class="step-name">Opening</h3>
        <div class="step-time">
            <div class="time">9:00 – 9:30</div>
            <div class="step"><img src="img/coffe.svg" alt="">Registration of participants and welcoming coffee</div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot part-in">9:30 – 9:45</div>
            <div class="part part2 border-bot part-in"></div>
            <div class="part part3 border-bot part-in">
                <p>Opening session: Welcoming speech from hosts</p>
            </div>
            <div class="part part4 border-bot part-in"></div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 part-in">9:45 – 10:30</div>
            <div class="part part2 part-in"></div>
            <div class="part part3 part-in">
                <p><span>Key Speaker</span></p>
            </div>
            <div class="part part4"></div>
        </div>
        <h3 class="step-name">Session 1</h3>
        <div class="parts-wrapper">
            <div class="part part1">10:30 – 11:15</div>
            <div class="part part2">
                <h4 class="text-left">An influential HR brand in agribusiness as part of a transformation for agricultural
                    companies in Ukraine<br> <span>(panel discussion)</span>
                </h4>
            </div>
            <div class="part part3">
                <h4 class="text-left">Tax and Financial Security Future<br> <span>(panel discussion)</span></h4>
            </div>
            <div class="part part4">
                <h4 class="text-left">Future of Fintech and Banking: Compliance Perspective</h4>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot "></div>
            <div class="part part2 border-bot ">
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Clément Coussens</span>, CEO, Agro KMR</p>
                <p><span>Yevgen Dudka</span>, owner, CEO, TM Viliya Group of Companies (pending confirmation)</p>
                <p><span>Svitlana Omelchenko</span>, CFO, Agromino (pending confirmation)</p>
                <img src="img/men-pic-1-2.svg" alt="">
                <p>Moderator: <br><span>Oleksandr Kaliuzhny</span>, analyst, UCAB</p>
            </div>
            <div class="part part3">
                <img src="img/men-pic-2.svg" alt="">
                <!--<p><span>Viktor Yushchenko</span>, President of Ukraine 2005-2009 (pending confirmation)</p>-->
                <p><span>Rolf Baron von Hohenhaou</span>, President of the European Association of Taxpayers</p>
                <p><span>Michael Jaeger</span>, Secretary General of the European Association of Taxpayers (pending confirmation)</p>
                <p><span>Grigol Katamadze</span>, President of the Ukrainian Association of Taxpayers</p>
                <img src="img/men-pic-2-1.svg" alt="">
                <p>Moderator: <br><span>Nataliya Ulyanova</span>, Partner, Deloitte Ukraine, Vice-President of the Ukrainian
                    Association of Taxpayers
                </p>
            </div>
            <div class="part part4 border-bot">
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Vira Savva</span>, Manager, Tax & Legal Department, Deloitte Ukraine</p>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 part-in">11:15 – 12:00</div>
            <div class="part part2 part-in">
                <h4 class="text-left">Sky, road, sea: search for investment opportunities in infrastructure</h4>
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Yuriy Golik</span> Counsellor to the Head ofthe Dnipropetrovsk Regional State Administration</p>
                <img src="img/men-pic-1-2.svg" alt="">
                <p>Moderator: <br><span>Dmitry Pavlenko</span>, Partner, Deloitte Ukraine</p>
            </div>
            <div class="part part3 part-in">

            </div>
            <div class="part part4 part-in">
                <h4 class="text-left">Brand and Reputation management</h4>
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Olga Dovganich</span>, Manager, Tax & Legal Department, Deloitte Ukraine</p>
            </div>
        </div>
        <h3 class="step-name">Session 2</h3>
        <div class="parts-wrapper">
            <div class="part part1">12:10 – 12:50</div>
            <div class="part part2">
                <h4 class="text-left">Export Potential of Ukrainian Companies <span>(panel discussion)</span></h4>
            </div>
            <div class="part part3">
                <h4 class="text-left">The future of Investment in Ukraine</h4>
            </div>
            <div class="part part4">
                <h4 class="text-left">Practical Approach to Creating a Company Strategy</h4>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot"></div>
            <div class="part part2 border-bot">
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Gregory Fishman</span>, Director, Business Development Department, Deloitte Ukraine</p>
                <p><span>Viktoria Chornovol</span>, Partner, Tax & Legal Department, Deloitte Ukraine </p>
                <p><span>Pim Haasdijk</span>, Managing Director, Green Seed Group</p>
            </div>
            <div class="part part3">
                <img src="img/men-pic-2.svg" alt="">
                <p><span>Matt Kennedy</span>, Kennedy Foundation Representative of the Black Rock Fund (pending confirmation)</p>
                <p><span>Michael Blazer</span>, Founder of the Blazer Foundation (pending confirmation)</p>
                <p><span>Tomasz Fiala</span>,  CEO Dragon Capital (pending confirmation)</p>
                <p><span>Olena Voloshyna</span>, Head of the representative office of the International Finance Corporation
                    (IFC) in Ukraine (pending confirmation)
                </p>
                <img src="img/men-pic-2-1.svg" alt="">
                <p>Moderator: <br><span>Givi Topchishvilli</span>, 9.8 Group, President</p>
            </div>
            <div class="part part4 border-bot">
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Andrew Kotyuzhinsky</span>, Director, Consulting Department, Deloitte in Ukraine</p>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 part-in">12:50 – 13:30</div>
            <div class="part part2 part-in">
                <h4 class="text-left">Money 3.0 Blockchain as a financial tool</h4>
                <img src="img/men-pic-2.svg" alt="">
                <p><span>Sergey Kalinin</span>, Investment Director, BlockBit Capital</p>
            </div>
            <div class="part part3 part-in">

            </div>
            <div class="part part4 part-in">
                <h4 class="text-left">Presentation of the results of the international research "Trends in the field of
                    personnel management - 2018"
                </h4>
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Liudmyla Korchak</span>, Сonsultant, Department of Consulting Deloitte in Ukraine</p>
            </div>
        </div>
        <div class="step-time">
            <div class="time">13:30 – 14:30</div>
            <div class="step"><img src="img/dinner.svg" alt="">Lunch break</div>
        </div>
        <h3 class="step-name">Session 3</h3>
        <div class="parts-wrapper">
            <div class="part part1">14:30 – 15:00</div>
            <div class="part part2">
                <h4 class="text-left">Transformations on pharmaceutical market</h4>
            </div>
            <div class="part part3">
                <h4 class="text-left">Ukrainian Innovation Leaders</h4>
            </div>
            <div class="part part4">
                <h4 class="text-left">On investment projects and attraction of debt financing</h4>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot"></div>
            <div class="part part2 border-bot">
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Eugene Zaika</span>, General director of Pharma Start</p>
                <p><span>Kostiantyn Kosyachenko</span>, PHD in Pharmaceutical Sciences, Associate Professor </p>
                <p><span>Volodymyr Kostyuk</span>, Executive Director, Farmak</p>
                <img src="img/men-pic-1-2.svg" alt="">
                <p>Moderator: <br><span>Nataliya Bukhalova</span>, CEO UMG</p>
            </div>
            <div class="part part3">
                <!--<img src="img/men-pic-2.svg" alt="">
                <p><span>Maksym Nefyodov</span>, Deputy Minister of Economic Development and Trade of Ukraine, Acting trade
                    representative of Ukraine (pending confirmation)
                <p><span>Andriy Pyshny</span>, Chairman of the Oschadbank Board (pending confirmation)</p>
                <p><span>Mariana Kaganiak</span>, Head of the Export Promotion Office, Advisor to the Minister of Economic
                    Development and Trade (pending confirmation)
                </p>-->
                <img src="img/men-pic-2-1.svg" alt="">
                <p>Moderator: <br><span>Denis Dovgopoly</span>, Managing Partner of BayView Innovations</p>
            </div>
            <div class="part part4 border-bot">
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Illya Segeda</span>, Senior Manager, Deloitte in Ukraine</p>
                <p><span>Taras Trokhymenko</span>, Senior Manager, Deloitte in Ukraine</p>
                <p><span>Andriy Putivskiy</span>, Head of the Corporate Customers Department, “Pravexbank”</p>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot part-in">15:00 – 15:30</div>
            <div class="part part2 border-bot part-in">
                <h4 class="text-left">Best Practices for the Transformation of Retail Business</h4>
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Sergey Bartoschuk</span>, Сhairman of the Cosmo Board of Supervisors</p>
                <p><span>Andrew Logvin</span>, CEO of Kasta.ua (pending confirmation)</p>
                <p><span>Igor Khizhnyak</span>, Comfy CEO(pending confirmation)</p>
                <p><span>?</span>, General Director of the chain of children's supermarkets
                    (pending confirmation)
                </p>
                <img src="img/men-pic-1-2.svg" alt="">
                <p>Moderator: <br><span>Alexander Sopronenkov</span>, Director, Deloitte Ukraine, Head of Industrial Group
                    of Retail, Wholesale and Distribution
                </p>
            </div>
            <div class="part part3 border-bot part-in">

            </div>
            <div class="part part4 border-bot part-in">
            </div>
        </div>
        <h3 class="step-name">Session 4</h3>
        <div class="parts-wrapper">
            <div class="part part1 border-bot">15:40 – 16:15</div>
            <div class="part part2 border-bot">
                <h4 class="text-left">Opportunities and trends in the field of “Green Energy”</h4>
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Arthur Oghajanyan</span>, Partner, Corporate Finance Department, Energy & Utilities Industrial
                    Group Leader, DeloitteUkraine
                </p>
            </div>
            <div class="part part3">
                <h4 class="text-left">Transformation cases of the companies in Ukraine</h4>

                <img src="img/men-pic-2-1.svg" alt="">
                <p>Moderator: <br><span>Andriy Bulakh</span>, Managing Partner, Deloitte Ukraine</p>
            </div>
            <div class="part part4 border-bot">
                <h4 class="text-left">Experience of successful investor attraction  under current conditions</h4>
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Yuriy Astakhov</span>, Director, Dragon Capital</p>
                <p><span>Andriy Gostyk</span>, Leading Banker, European Bank for Reconstruction and Development</p>
                <p><span>?</span>, Representative of a private equity fund (pending confirmation)</p>
                <img src="img/men-pic-3.svg" alt="">
                <p>Moderator: <br><span>Dmytro Anufriiev</span>, CFA, Partner, Financial Advisory Department, Deloitte Ukraine</p>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 part-in">16:15 – 17:00</div>
            <div class="part part2 part-in">
                <h4 class="text-left">Working for private capital: honestly about successes and failures</h4>
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Andriy Servetnyk</span>, Partner, Tax & Legal Department, Deloitte Ukraine</p>
            </div>
            <div class="part part3 part-in">

            </div>
            <div class="part part4 part-in">
                <h4 class="text-left">Top 10 legal fuckups of a fast growing business. Work on the bugs.</h4>
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Kateryna Grabovyk</span>, Senior Manager, Tax & Legal Department, Deloitte Ukraine</p>
            </div>
        </div>
        <div class="step-time">
            <div class="time">17:00 – 21:00</div>
            <div class="step"><img src="img/coctel.svg" alt="">Cocktail break</div>
        </div>
    </div>
</section>