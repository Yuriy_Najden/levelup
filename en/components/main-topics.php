<section class="main-topics">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Forum program</h2>
                <div class="flow">
                    <div class="top">
                        <img src="img/flow-1-pic.png" class="pic">
                        <div class="text">
                            <h3>Main stage</h3>
                            <p>We will discuss why and how the global trends and inner drives prompt the Ukrainian business
                                to change.
                            </p>
                            <p>You will learn about the requirements of international investors to the Ukrainian business,
                                including the current and expected changes in tax legislation. You will also learn about
                                the successful cases of transformational changes from the market leaders.
                            </p>
                            <a href="#" class="flow-open active-flow" data-toggle="collapse" data-target="#fl-1-mor">Learn more</a>
                        </div>
                    </div>
                    <div class="bottom collapse" id="fl-1-mor">
                        <div class="items-wrapper">
                            <p>Transformation as an imperative of our time</p>
                            <p>Macroeconomic background</p>
                            <p>Tax system now and in the future</p>
                            <p>What foreign investors are ready and willing to invest in?</p>
                        </div>
                    </div>
                </div>
                <div class="flow">
                    <div class="top">
                        <img src="img/flow-2-pic.png" class="pic">
                        <div class="text">
                            <h3>Transformation session: Industrial</h3>
                            <p>Our master classes will be attended by representatives of successful Ukrainian companies
                                operating in agricultural, infrastructural, and pharmaceutical sectors. We will talk about
                                market transformations, analyze the cases of successful company development through the
                                promotion of investments. During the session the experts will assess the Ukrainian companies'
                                export potential and explain what steps should be taken to reach international markets.
                            </p>
                            <a href="#" class="flow-open active-flow" data-toggle="collapse" data-target="#fl-2-mor">Learn more</a>
                        </div>
                    </div>
                    <div class="bottom collapse" id="fl-2-mor">
                        <div class="items-wrapper">
                            <p>Steps for entering the European retail network</p>
                            <p>International market opportunities for agricultural, infrastructural, and farmaceutical companies</p>
                            <p>Wealth management advice</p>
                            <p>IT trends and company development tools</p>
                        </div>
                    </div>
                </div>
                <div class="flow">
                    <div class="top">
                        <img src="img/flow-3-pic.png" class="pic">
                        <div class="text">
                            <h3>Transformation session: Functional</h3>
                            <p>Experience-based and packed with practical insights master classes from world experts. They
                                will share hands-on recommendations on how to transform your company from within, in particular,
                                how to structure work, build up a flexible mobile staff bound by efficient relations. Our
                                discussions will cover issues related to opening accounts abroad, refinancing, capital raising,
                                and M&A agreements. There will also be a discussion of business plan as an in-house fund
                                management tool.
                            </p>
                            <a href="#" class="flow-open active-flow" data-toggle="collapse" data-target="#fl-3-mor">Learn more</a>
                        </div>
                    </div>
                    <div class="bottom collapse" id="fl-3-mor">
                        <div class="items-wrapper">
                            <p>Compliance procedure and transparency in business</p>
                            <p>The most important for business in banking, legal, and financial sectors</p>
                            <p>Reputation management</p>
                            <p>Human Capital Trends and millenials</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>