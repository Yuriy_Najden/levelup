<section class="video-join">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">We invite you to join us</h2>
                <div class="controls-wrapper">
                    <a href="#" class="control prev stop-video">
                        <svg width="29" height="22" viewBox="0 0 29 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.878 1L2 11.1124L11.878 21" stroke="#fff" stroke-width="2"/>
                            <path d="M29 11L2.65854 11" stroke="#fff" stroke-width="2"/>
                        </svg>
                    </a>
                    <a href="#" class="control next stop-video">
                        <svg width="29" height="22" viewBox="0 0 29 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M17.122 1L27 11.1124L17.122 21" stroke="#fff" stroke-width="2"/>
                            <path d="M0 11L26.3415 11" stroke="#fff" stroke-width="2"/>
                        </svg>
                    </a>
                </div>
                <div class="video-slider" id="video-slider">
                    <div class="slide">
                        <p class="slide-number">1/2</p>
                        <div class="video-wrapper">
                            <div class="video">
                                <div class="youtube" id="XUNu15SqB8U"></div>
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <p class="slide-number">2/2</p>
                        <div class="video-wrapper">
                            <div class="video">
                                <div class="youtube" id="3Av2_wPuO4A"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>