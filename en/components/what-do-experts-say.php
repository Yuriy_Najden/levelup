<section class="what-do-experts-say">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">What experts say</h2>
                <div class="controls-wrapper">
                    <a href="#" class="control prev"></a>
                    <a href="#" class="control next"></a>
                </div>
                <div class="experts-say-slider" id="experts-say-slider">
                    <div class="slide">
                        <div class="text">
                            <img src="img/qote-on.svg" alt="" class="quotes quotes-top">
                            <img src="img/qote-off.svg" alt="" class="quotes quotes-bottom">
                            <div>
                                <p>This is what any company striving to keep its business going, to scale up and expand,
                                    to attract investors, or even just to survive, needs.
                                </p>
                                <p>All these trends are in full swing in Ukraine, and we are joining in many of them, albeit
                                    on a framework basis. This spurs company owners to active actions.
                                </p>
                                <p>Either you change or your business will cease to exist.</p>
                            </div>
                        </div>
                        <div class="expert">
                            <div class="slide-number">
                                1/4
                            </div>
                            <img src="pic/spick-pic-7.jpg" alt="">
                            <h3>Nataliya Ulyanova</h3>
                            <p>Tax & Legal Partner at Deloitte Ukraine, Complex Business Transformation Leader
                            </p>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="text">
                            <img src="img/qote-on.svg" alt="" class="quotes quotes-top">
                            <img src="img/qote-off.svg" alt="" class="quotes quotes-bottom">
                            <div>
                                <p>As usual, we have prepared only practical things presented in a concise way. In addition,
                                    topical issues of today are also included in the agenda. You will hear incredibly interesting
                                    people from Ukraine and the world, reporting on the main stage. All of them are renowned
                                    for their success in transformations at both business and country levels. This year
                                    we have launched industrial workshops where you will learn about the specifics of
                                    transformation implementation in the relevant market segment from those directly involved
                                    in the change process.
                                </p>
                                <p>This year we are planning something revolutionary and even of a larger scale. The participants
                                    are invited to three sessions held in several formats: presentation of the draft Tax
                                    Code of Ukraine and discussion of the key trends and changes in the Ukrainian legislation
                                    and business landscape (on the main stage), and two simultaneous streams of industrial
                                    workshops given by industry experts to share their insights into real business transformation cases.
                                </p>
                                <p> The heavy agenda will be tempered with a startup alley and an evening mix of inspiring
                                    music and valuable networking.
                                </p>
                            </div>
                        </div>
                        <div class="expert">
                            <div class="slide-number">
                                2/4
                            </div>
                            <img src="pic/spick-pic-2.jpg" alt="">
                            <h3>Grigol Katamadze</h3>
                            <p>President of Taxpayers Association of Ukraine</p>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="text">
                            <img src="img/qote-on.svg" alt="" class="quotes quotes-top">
                            <img src="img/qote-off.svg" alt="" class="quotes quotes-bottom">
                            <div>
                                <p>According to Deloitte's report, one of the most remarkable results of Industry 4.0 is
                                    the strengthening of the role of business in the areas where the state used to be in
                                    the driving seat or even a monopolist.
                                </p>
                                <p>Indeed, business has its own resources allowing it to impact suppliers and customers.
                                    Business is more flexible than the state and more organized than the community. However,
                                    more influence means more responsibility.
                                </p>
                                <p>In this context, business cannot afford itself to stay uninvolved. Is it ready for such
                                    changes? Is it ready to maintain the standards of transparency, integrity, social
                                    responsibility, environmental awareness, and corporate values?
                                </p>
                                <p>Changes do not germinate in the governmental offices, they will rather emerge in the
                                    minds of specific people or come from their daily behavior.
                                </p>
                            </div>
                        </div>
                        <div class="expert">
                            <div class="slide-number">
                                3/4
                            </div>
                            <img src="pic/spick-pic-9.jpg" alt="">
                            <h3>Andriy Bulakh</h3>
                            <p>Managing Partner at Deloitte Ukraine</p>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="text">
                            <img src="img/qote-on.svg" alt="" class="quotes quotes-top">
                            <img src="img/qote-off.svg" alt="" class="quotes quotes-bottom">
                            <div>
                                <p>I invite you to join the Level Up Ukraine community!</p>
                                <p>This year the Forum will focus on the trends and changes in taxation, which are the main
                                    drivers of changes and cannot but affect both business and the state.
                                </p>
                                <p>Today’s challenges require us to change our practices and approaches to business.
                                    Transformations are not just well-timed in our country and in today’s business environment,
                                    they have already evolved into a powerful trend, and riding on the wave of its momentum
                                    we are capable of achieving a lot, I am sure.
                                </p>
                            </div>
                        </div>
                        <div class="expert">
                            <div class="slide-number">
                                4/4
                            </div>
                            <img src="pic/spick-pic-35.jpg" alt="">
                            <h3>Alexander Cherinko</h3>
                            <p>Partner, Head of Tax & Legal at Deloitte Ukraine </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>