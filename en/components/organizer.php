<section class="organizer">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Level Up Ukraine 2018 Hosts</h2>

                <div class="item top">
                    <h3>Deloitte</h3>
                    <p>Deloitte is a brand that employs over 263 thousand professionals working in our offices in 150
                        countries and providing audit and assurance, tax and legal, financial advisory, risk advisory,
                        and consulting services.
                    </p>
                    <p>Over 150 years of dedicated work and commitment to make the world better have greatly contributed
                        to the growth of Deloitte's global network in terms of size and diversity.
                    </p>
                    <p>Deloitte is one of the Big Four accounting firms and the largest professional services network by
                        number of employees.
                    </p>
                    <p>In 2017 Fortune named Deloitte to its Fortune 100 list.</p>
                    <p>Deloitte Ukraine's complex transformation team is proving to be number one trusted advisor for
                        companies on their way to growth and development.
                    </p>
                </div>
                <div class="item">
                    <h3>Taxpayers Association of Ukraine</h3>
                    <p>(TAU) is the largest and most influential all-Ukrainian non-governmental organization in the field
                        of taxes and finance. It has local centers across Ukraine and is a member of the World and Europe
                        Taxpayers Associations.
                    </p>
                    <p>For 20 years, the Association has been representing and protecting the rights of conscientious
                        taxpayers, and focuses on improving the business climate in the country, developing partnership
                        between the government and business, and increasing social responsibility of citizens of the country.
                        The TAU takes active steps to maintain a dialog with parliamentarians and government officials,
                        works in close cooperation with tax authorities, supports broad international relations, organizes
                        training, as well as provides consultations and exchange of experience.
                    </p>
                </div>
                <img src="img/organizer-pic.png" alt="" class="pic">
            </div>
        </div>
    </div>
</section>