<section class="our-speakers" id="speakers">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Forum speakers</h2>
                <div class="items-wrapper">
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-42.jpg" alt="">
                        </div>
                        <img src="img/ger-flag.svg" alt="" class="flag">
                        <h3>Rolf Baron von Hohenhaou</h3>
                        <p>President of the European Association of Taxpayers</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-19.jpg" alt="">
                        </div>
                        <img src="img/usa-flag.svg" alt="" class="flag">
                        <p class="none">(not confirmed)</p>
                        <h3>Givi Topchishvili</h3>
                        <p>Founder and President of 9.8 Group</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-9.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Andriy Bulakh</h3>
                        <p>Managing Partner, Deloitte Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-2.jpg" alt="">
                        </div>
                        <img src="img/gr-flag.svg" alt="" class="flag">
                        <h3>Grigol Katamadze</h3>
                        <p>President of the Ukrainian Association of Taxpayers</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-4.jpg" alt="">
                        </div>
                        <img src="img/fra-flag.svg" alt="" class="flag">
                        <h3>Clément Coussens</h3>
                        <p>CEO, Agro KMR</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-7.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Nataliya Ulyanova</h3>
                        <p>Partner, Deloitte Ukraine, Vice-President of the Ukrainian Association of Taxpayers</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-43.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Sergey Kalinin</h3>
                        <p>Investment Director, BlockBit Capital</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-8.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Eugene Zaika</h3>
                        <p>"CIS Regional Head of Acino Group (Switzerland) General Director of Pharma Start LLC, Ukraine"</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-30.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Nataliya Bukhalova</h3>
                        <p>CEO UMG</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-32.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Yuriy Astakhov</h3>
                        <p>Director, Dragon Capital </p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/nid-flag.svg" alt="" class="flag">
                        <h3>Pim Haasdijk</h3>
                        <p>Managing Director, Green Seed Group</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Denis Dovgopoly</h3>
                        <p>Managing Partner of BayView Innovations</p>
                    </div>
                </div>
                <div class="items-wrapper more-speakers">
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-33.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Svitlana Dryhush</h3>
                        <p>Investment director, Horizon Capital </p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-34.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Konstantin Magaletskyi</h3>
                        <p>Partner, Horizon Capital</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Volodymyr Kosyuk</h3>
                        <p>Executive Director of Farmak Company</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-35.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Alexander Cherinko </h3>
                        <p>"Head of Tax and Legal Department, Partner,
                            Deloitte Ukraine"</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-27.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Viktoria Chornovol</h3>
                        <p>Partner, Tax & Legal Department, Deloitte Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-26.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Artur Ohadzhanyan </h3>
                        <p>Partner of Deloitte Financial Advisory Department in Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-28.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Gregory Fishman</h3>
                        <p>Director, Head of Business Development, Deloitte Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-11.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Andriy Servetnyk</h3>
                        <p>Partner, Tax & Legal Department, Deloitte Ukraine</p>
                    </div>

                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-36.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Dmytro Anufriiev</h3>
                        <p>CFA, Partner, Deloitte, Financial Advisory</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-37.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Andrew Kotyuzhinsky</h3>
                        <p>Director, Department of Consulting Deloitte in Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Liudmyla Korchak</h3>
                        <p>Сonsultant, Department of Consulting Deloitte in Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-24.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Oleksandr Kaliuzhny</h3>
                        <p>Analyst, UCAB</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-10.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Kostiantyn Kosyachenko</h3>
                        <p>PHD in Pharmaceutical Sciences, Associate Professor</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Andriy Gostyk</h3>
                        <p>"Leading Banker, European Bank for Reconstruction and Development"</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Andriy Putivskiy</h3>
                        <p>"Leading Banker, European Bank for Reconstruction and Development"</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-31.jpg" alt="">
                        </div>
                        <img src="img/ch-flag.svg" alt="" class="flag">
                        <h3>Yuri Golik</h3>
                        <p>"Advisor to the head of the Dnipropetrovsk Regional State Administration"</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-38.jpg" alt="">
                        </div>
                        <img src="img/ch-flag.svg" alt="" class="flag">
                        <h3>Illya Segeda,</h3>
                        <p>Senior Manager, Deloitte in Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-39.jpg" alt="">
                        </div>
                        <img src="img/ch-flag.svg" alt="" class="flag">
                        <h3>Taras Trokhymenko</h3>
                        <p>Senior Manager, Deloitte in Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-40.jpg" alt="">
                        </div>
                        <img src="img/ch-flag.svg" alt="" class="flag">
                        <h3>Kateryna Grabovyk</h3>
                        <p>"Senior Manager, Tax & Legal Department, Deloitte in Ukraine"</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-41.jpg" alt="">
                        </div>
                        <img src="img/ch-flag.svg" alt="" class="flag">
                        <h3>Nataliia Kronik</h3>
                        <p>Manager, Tax & Legal Department, Deloitte Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-5.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Vira Savva</h3>
                        <p>Manager, Tax & Legal Department, Deloitte Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-6.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Olga Dovganich</h3>
                        <p>Manager, Tax & Legal Department, Deloitte Ukraine</p>
                    </div>
                    <!--<div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-12.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(not confirmed)</p>
                        <h3>Viktor Yushchenko</h3>
                        <p>President of Ukraine 2005-2009</p>
                    </div>-->
                    <div class="item"">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-13.jpg" alt="">
                        </div>
                        <img src="img/ger-flag.svg" alt="" class="flag">
                        <p class="none">(not confirmed)</p>
                        <h3>Michael Jaeger</h3>
                        <p>Secretary General of the European Association of Taxpayers</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-14.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(not confirmed)</p>
                        <h3>Yevgen Dudka</h3>
                        <p>Owner, CEO, TM Viliya Group of Companies</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-15.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(not confirmed)</p>
                        <h3>Svitlana Omelchenko</h3>
                        <p>CFO, Agromino</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/usa-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Matt Kennedy</h3>
                        <p>Kennedy Foundation</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-16.jpg" alt="">
                        </div>
                        <img src="img/usa-flag.svg" alt="" class="flag">
                        <p class="none">(not confirmed)</p>
                        <h3>Michael Blazer</h3>
                        <p>Founder of the Blazer Foundation</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-17.jpg" alt="">
                        </div>
                        <img src="img/ch-flag.svg" alt="" class="flag">
                        <p class="none">(not confirmed)</p>
                        <h3>Tomasz Fiala</h3>
                        <p>CEO Dragon Capital</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-18.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(not confirmed)</p>
                        <h3>Olena Voloshyna</h3>
                        <p>Head of IFC Representative. Office in Ukraine</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-22.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(not confirmed)</p>
                        <h3>Mariana Kaganiak</h3>
                        <p>Head of the Export Promotion Office, Advisor to the Minister of Economic Development and Trade</p>
                    </div>

                </div>
                <a href="#" class="open-more-speakers" id="open-more-speakers">All speakers</a>
            </div>

        </div>
    </div>
</section>