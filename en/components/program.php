<section class="program" id="program">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Learn about the program Level Up Ukraine 2018</h2>
                <p>It is hard to overstate the significance of transformations and the need to create favorable conditions
                    for business development in Ukraine. The forum program covers the most relevant topics and offers master
                    classes from influential individuals.
                </p>
                <a href="#" class="button mob-btn" download="upload/program.pdf">Download program</a>
                <a href="#" class="desc-btn active-program" id="btn-program-open" data-toggle="collapse" data-target="#program-more">Know the program</a>
                <img src="img/program-bg-pic.png" alt="" class="pic">
            </div>

        </div>
    </div>
</section>