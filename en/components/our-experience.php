<section class="our-experience">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Our experience in organizing large-scale forums for businesses</h2>
                <p>Each year, we gather key market players, investors, regulators, and experts to discuss the changing
                    climate of transformations in Ukraine.  Some of our previous speakers have been: Grigol Katamadze,
                    Arturo Briss, Victoria Tihipko, Nina Yuzhanina, and many other notable names from business, taxation,
                    and investment circles.
                </p>
            </div>
        </div>
    </div>
</section>