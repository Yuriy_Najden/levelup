<section class="participation-packages" id="participation-packages">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Forum participation fee</h2>
                <div class="items-wrapper">
                    <div class="item">
                        <div class="sale"></div>
                        <h3 class="pack-name">Basic</h3>
                        <p class="pack-price">2900 UAH</p>
                        <ul>
                            <li>forum accreditation</li>
                            <li>main stage access</li>
                            <li>participant package</li>
                            <li>coffee breaks</li>
                            <li>dinner</li>
                            <li>cocktail jazz</li>
                            <li>video recording of all presentations made by the forum speakers will be sent to your email</li>
                        </ul>
                        <div class="buttons">
                            <a href="#" class="button" data-pakege="Basic" data-toggle="modal" data-target="#ticketModal">Apply here</a>
                            <a href="#" class="booking" data-pakege="Basic" data-toggle="modal" data-target="#ticketModalBooking">Book a ticket</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="sale">
                            <img src="img/sale.svg" alt="">
                            <p>15% off when booking three tickets or more</p>
                        </div>
                        <h3 class="pack-name">Master-class</h3>
                        <p class="pack-price">4900 UAH</p>
                        <ul>
                            <li>forum accreditation</li>
                            <li>main stage access</li>
                            <li>participant package</li>
                            <li>coffee breaks</li>
                            <li>dinner</li>
                            <li>cocktail jazz</li>
                            <li>video recording of all presentations made by the forum speakers will be sent to your email</li>
                        </ul>
                        <div class="buttons master-panel">
                            <a href="#" class="button" data-pakege="Master-class" data-toggle="modal" data-target="#ticketModal">Apply here</a>
                            <a href="#" class="booking" data-pakege="Master-class" data-toggle="modal" data-target="#ticketModalBooking">Book a ticket</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="sale">
                            <img src="img/sale.svg" alt="">
                            <p>15% off when booking three tickets or more</p>
                        </div>
                        <h3 class="pack-name">Master-class Plus</h3>
                        <p class="pack-price">5900 UAH</p>
                        <ul>
                            <li>forum accreditation</li>
                            <li>main stage access</li>
                            <li>participant package</li>
                            <li>coffee breaks</li>
                            <li>dinner</li>
                            <li>cocktail jazz</li>
                            <li>video recording of all presentations made by the forum speakers will be sent to your email</li>
                        </ul>
                        <div class="buttons">
                            <a href="#" class="button" data-pakege="Master-class Plus" data-toggle="modal" data-target="#ticketModal">Apply here</a>
                            <a href="#" class="booking" data-pakege="Master-class Plus" data-toggle="modal" data-target="#ticketModalBooking">Book a ticket</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>