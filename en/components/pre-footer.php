<section class="pre-footer">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <div class="documents">
                    <a href="https://www2.deloitte.com/ua/uk/footerlinks1/privacy.html?icid=bottom_privacy" target="_blank">Confidentiality Policy</a>
                    <a href="https://www2.deloitte.com/ua/uk/footerlinks1/legal.html?icid=bottom_legal" target="_blank">Public offer</a>
                </div>
                <div class="soc-wrapper">
                    <a href="https://www2.deloitte.com/ua/uk.html" class="ofs" target="_blank">deloitte.com</a>
                    <a href="https://www.facebook.com/levelupukraine/" class="fb" target="_blank"></a>
                    <a href="https://www.linkedin.com/company/level-up-ukraine/" class="in" target="_blank"></a>
                    <a href="https://www.youtube.com/channel/UCORXBcCw7VSPVTAxBccPU6g" class="you" target="_blank"></a>
                </div>
            </div>
        </div>
    </div>
    <a href="#" class="go-top up-btn"></a>
</section>