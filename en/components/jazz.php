<section class="jazz">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Business, transformations, investment, and jazz</h2>
                <img src="img/jazz.png" alt="" class="pic">
                <p>In keeping with the best traditions of an all-round networking, all participants will be invited to a
                    jazz party that will wrap up the eventful forum program and feature musical performances of the talented
                    jazz band.
                </p>
            </div>
        </div>
    </div>
</section>