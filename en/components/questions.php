<section class="questions">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Have questions?</h2>
                <p>Please submit a completed form and we will contact you shortly</p>
                <form role="form" data-toggle="validator"  method="post" action="send.php">
                    <div class="form-col">
                        <div class="form-group inp-name has-feedback">
                            <label for="q-name">Name</label>
                            <input type="text" class="form-control" name="name" id="q-name" placeholder="Enter your name" required>
                        </div>
                        <div class="form-group inp-phone has-feedback">
                            <label for="q-tel">Phone</label>
                            <input type="tel" class="form-control" name="phone" id="q-tel" placeholder="Enter your phone" required>
                        </div>
                        <div class="form-group inp-email has-feedback">
                            <label for="q-email">Email</label>
                            <input type="email" class="form-control" name="email" id="q-email" placeholder="Enter your email" required>
                        </div>
                    </div>
                    <div class="form-col">
                        <div class="form-group textarea-group inp-text has-feedback">
                            <label for="q-text">Question</label>
                            <textarea id="q-text" name="question" class="form-control" placeholder="Enter your question" required></textarea>
                        </div>

                        <button type="submit" class="button">Send</button>
                    </div>
                    <input type="hidden" name="lid-name" value="Вопрос с сайта">
                </form>
                <img src="img/question-pic.png" alt="" class="pic">
            </div>

        </div>
    </div>
</section>