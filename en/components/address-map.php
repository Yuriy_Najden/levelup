<section class="address-map">
    <div class="address">
        <h2 class="block-title">Time and venue</h2>
        <div class="item">
            <img src="img/datapiker.svg" alt="">
            <p>November 28, 2018 <br> 9:00</p>
        </div>
        <div class="item">
            <img src="img/place-icon.svg" alt="">
            <p>Kyiv, Naberezhno-Khreschatytska <br> 1 Fairmont Grand Hotel Kyiv</p>
        </div>
    </div>
    <div class="map" id="map"></div>
</section>