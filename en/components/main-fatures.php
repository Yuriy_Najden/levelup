<section class="main-features">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Six major opportunities for participants Level Up Ukraine 2018</h2>
                <div class="items-wrapper">
                    <div class="item">
                        <p class="border-text">01</p>
                        <p class="text">To visit groundbreaking master classes on business transformation</p>
                    </div>
                    <div class="item">
                        <p class="border-text">02</p>
                        <p class="text">To prepare for the changes that challenge business in Ukrainian and international
                            taxation
                        </p>
                    </div>
                    <div class="item">
                        <p class="border-text">03</p>
                        <p class="text">To learn more about the market of the future: what it will be like and what preparations
                            should be made right away
                        </p>
                    </div>
                    <div class="item">
                        <p class="border-text">04</p>
                        <p class="text">To gain insights from the world transformation leaders</p>
                    </div>
                    <div class="item">
                        <p class="border-text">05</p>
                        <p class="text">To discuss real transformation cases and international trends</p>
                    </div>
                    <div class="item">
                        <p class="border-text">06</p>
                        <p class="text">To meet reputable business owners and investors</p>
                    </div>
                </div>
                <a href="#participation-packages" class="button scroll-to">Book a ticket</a>
            </div>
        </div>
    </div>
</section>