<section class="gallery">
    <div class="gallery-slider" id="gallery-slider">
        <a href="pic/gallery/galleri-pic-1.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-1.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-2.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-2.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-3.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-3.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-4.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-4.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-5.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-5.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-6.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-6.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-7.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-7.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-8.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-8.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-9.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-9.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-10.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-10.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-11.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-11.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-12.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-12.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-13.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-13.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-14.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-14.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-15.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-15.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-16.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-16.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-17.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-17.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-18.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-18.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-19.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-19.jpg" alt="">
        </a>
        <a href="pic/gallery/galleri-pic-20.jpg" data-fancybox="images1" class="slide">
            <img src="pic/gallery/galleri-pic-20.jpg" alt="">
        </a>
    </div>
</section>