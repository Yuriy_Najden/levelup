<section class="business-trend">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Business transformation</h2>
                <h3>Today, business is faced with many challenges: the emergence of new business models, digitization,
                    globalization of sales markets, increased competition for staff and capital, increased regulatory
                    requirements, and increased transparency of financial transactions. Today, business is faced with many
                    challenges: the emergence of new business models, digitalization, sales market globalization, growing
                    competition for personnel and capital, stricter regulatory requirements, and higher transparency of
                    financial transactions. According to experts, the pressure of these and other factors will lead to the
                    reduction of a company's average lifespan from 60 to just 15 years. Only those who change with the
                    times will become the true survivors. For these reasons, this year's forum is dedicated to business
                    transformation.
                </h3>
                <div class="items-wrapper">
                    <div class="item">
                        <img src="img/business-life-pic-1.svg" alt="">
                        <div class="text">
                            <h4>Practicable:</h4>
                            <p>for already established companies and businesses in the making</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/business-life-pic-2.svg" alt="">
                        <div class="text">
                            <h4>Relevant:</h4>
                            <p>for investors and business owners</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/business-life-pic-3.svg" alt="">
                        <div class="text">
                            <h4>Interesting:</h4>
                            <p>to top-management and officials</p>
                        </div>
                    </div>
                </div>
                <div class="buttons-wrapper">
                    <a href="#participation-packages" class="button scroll-to">Apply here</a>
                    <a href="#program" class="button scroll-to">Forum program</a>
                </div>
            </div>
        </div>
    </div>
</section>