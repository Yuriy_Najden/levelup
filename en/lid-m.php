<!DOCTYPE html>
<html lang="ua">
    <head>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>

        <meta property="og:title" content="" />
        <meta property="og:description" content="" />
        <meta property="og:type" content="video.movie" />
        <meta property="og:url" content="" />
        <meta property="og:image" content="/img/og.png" />

        <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
        <link rel="manifest" href="img/favicon/manifest.json">

        <meta name="msapplication-TileColor" content="#000000">

        <meta name="theme-color" content="#000000">

        <title>LevelUp</title>

        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/jquery.fancybox.css">
        <link type="text/css" rel="stylesheet" href="css/style.css"/>

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-W44ZTV8');</script>
        <!-- End Google Tag Manager -->

    </head>
    <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W44ZTV8"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
        <div class="wrapper lidmagnit-page">
            <div class="loader" id="loader"></div>

            <header>
                <div class="fix-menu">
                    <div class="logo-phone">
                        <div class="container">
                            <div class="row">
                                <div class="content col-md-12">
                                    <div class="logo-wrapper">
                                        <img src="img/logo.svg" alt="" class="logo">
                                        <div class="line"></div>
                                        <p>Level Up Ukraine 2018</p>
                                    </div>
                                    <div class="phone-wrapper">
                                        <a href="tel:08001111111" class="phone">0 800 211 213</a>
                                        <a href="#" class="callback">Book a call back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav>
                        <div class="container">
                            <div class="row">
                                <div class="content col-md-12">
                                    <div class="lang">
                                        <a href="#" class="active-lang">Українською</a>
                                        <div class="line"></div>
                                        <a href="#">English</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>

                <div class="header-content">
                    <div class="container">
                        <div class="row">
                            <div class="content col-md-12">
                                <div class="text">
                                    <h1>Нові виклики та трансформації для компаній у процесі зростання</h1>
                                    <p>Команда Комплексних трансформацій бізнесу Deloitte детально розкриває сутність поняття
                                        трансформації для бізнесу.
                                    </p>
                                    <p>Дізнайтеся, які є драйвери трансформації бізнесу, навіщо бізнесу взагалі трансформуватися
                                        та яких проблем можна уникнути, і чого досягнути завдяки трансформації.
                                    </p>
                                    <p>Презентація від організатора форуму Level Up Ukraine 2018, компанії що входить у «велику
                                        четвірку» аудиторських компаній - Deloitte.
                                    </p>
                                    <a href="index.php" class="button" target="_blank">Дізнатись більше</a>
                                </div>
                                <div class="form-wrapper">
                                    <p>Отримайте матеріали та практичні поради для розвитку своєї компанії</p>
                                    <form role="form" data-toggle="validator" >
                                        <div class="form-group inp-name has-feedback">
                                            <label for="q-name">Ім’я</label>
                                            <input type="text" class="form-control" id="q-name" placeholder="Введіть ваше ім’я" required>
                                        </div>
                                        <div class="form-group inp-phone has-feedback">
                                            <label for="q-tel">Телефон</label>
                                            <input type="tel" class="form-control" id="q-tel" placeholder="Введіть ваш телефон" required>
                                        </div>
                                        <div class="form-group inp-email has-feedback">
                                            <label for="q-email">Email</label>
                                            <input type="email" class="form-control" id="q-email" placeholder="Введіть ваш email" required>
                                        </div>
                                        <button type="submit" class="button">Надіслати на email</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <?php include_once 'components/pre-footer.php'?>

            <?php include_once 'components/footer.php'?>

        </div>

        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--[if lt IE 7]>
        <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
        <![endif]-->
        <!--[if lt IE 8]>
        <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
        <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
        <![endif]-->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <!--<script src="js/jquery-3.2.1.min.js"></script>-->

        <script src="js/bootstrap.min.js"></script>

        <script src="js/validator.min.js"></script>
        <script src="js/jquery.maskedinput.min.js"></script>
        <script src="js/js.js"></script>

    </body>
</html>