$(function() {

    // ANIMATE INIT

    //new WOW().init();

    // MAP INIT


    function initMap() {
           var location = {
                lat: 50.4603606,
                lng: 30.525481
           };

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: location,
                scrollwheel: false,
                mapTypeControl: false,
                zoomControl: false
            });

            var marker = new google.maps.Marker({
                position: location,
                map: map,
                icon: {
                    url: ('img/map-marker.svg'),
                    scaledSize: new google.maps.Size(15, 21)
                }
            });

        $.getJSON("map_style.json", function(data) {
            map.setOptions({styles: data});
        });

    }

    initMap();

    $(document).on('click', '.up-btn', function() {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    //SCROLL MENU

   $(document).on('click', '.scroll-to', function (e) {
       e.preventDefault();

       var href = $(this).attr('href');

       $('html, body').animate({
           scrollTop: $(href).offset().top
       }, 1000);

   });

    // CASTOME SLIDER ARROWS

   $('#experts-say-slider').slick({
       autoplay: false,
       autoplaySpeed: 5000,
       slidesToShow: 1,
       slidesToScroll: 1,
       arrows: false,
       fade: true,
       adaptiveHeight: true
   });

   $('.what-do-experts-say .prev').click(function(e){
       e.preventDefault();

       $('#experts-say-slider').slick('slickPrev');
   });

   $('.what-do-experts-say .next').click(function(e){
       e.preventDefault();

       $('#experts-say-slider').slick('slickNext');
   });

   //

    $('#gallery-slider').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 1,
        centerPadding: '25%',
        slidesToScroll: 1,
        centerMode: true,
        arrows: false,
        lazyLoad: 'ondemand',
        fade: false,
        responsive: [
            {
                breakpoint: 540,
                settings: {
                    centerPadding: '15%'
                }
            }
        ]

    });

    $('[data-fancybox]').fancybox({
        protect: true
    });
    //

    $('form').validator();

    // DTA VALUE REPLACE

    $('#participation-packages .button').on('click', function () {

        var pakege = $(this).data('pakege');
        $('#ticketModal .pac-name').text(pakege);
        $('#ticketModal').find('input[name=package]').val(pakege);
    });

    $('#participation-packages .booking').on('click', function () {

        var pakege = $(this).data('pakege');
        $('#ticketModalBooking .pac-name').text(pakege);
        $('#ticketModalBooking').find('input[name=package]').val(pakege);

    });

    $('#SP-type .button').on('click', function() {

        var fname = $(this).data('fname');
        var teg = $(this).data('teg');

        $('#SP-modal .pac-name').text(fname);
        $('#SP-modal-2 .pac-name').text(fname);
        $('#SP-modal-2').find('input[name=teg]').val(teg);
        /*$('#SP-modal').find('input[name=lid-name]').val(ftype);*/
    });

    //

    $('#participation-packages .master-panel .button').on('click', function () {
        $('#ticketModal').addClass('master-custom-on');
    });

    $('#participation-packages .master-panel .booking').on('click', function () {
        $('#ticketModalBooking').addClass('master-custom-on');
    });

    //

    $('#ticketModal').on('hidden.bs.modal', function () {
        $('#ticketModal').removeClass('master-custom-on');
    });

    $('#ticketModalBooking').on('hidden.bs.modal', function () {
        $('#ticketModalBooking').removeClass('master-custom-on');
    });



    // PHONE MASK

    $("input[type=tel]").mask("+38(999) 999-99-99");


    //

    $('#menu-btn').on('click', function (e) {
        e.preventDefault();

        $(this).toggleClass('active-menu');
        $('#mob-menu').toggleClass('active-mob-menu');
    });

    $('#mob-menu .scroll-to').on('click', function () {
        $('#menu-btn').toggleClass('active-menu');
        $('#mob-menu').toggleClass('active-mob-menu');
    });

    //

    $('#open-more-speakers').on('click', function (e) {
       e.preventDefault();

       $(this).fadeOut(400);
       $('.more-speakers').slideDown(400);
       $('.more-speakers').css({'display':'flex'});
    });

    //

    $('#btn-program-open').on('click', function (e) {
        e.preventDefault();

        $(this).toggleClass('active-program');
    });

    //

    $('.flow-open').on('click', function (e) {
       e.preventDefault();

        $(this).toggleClass('active-flow');
    });

    $(".modal").on("show.bs.modal", function () {
        $(this).css('display','flex');
    });

    //

    setTimeout(function() {
        $('.cookie-container').slideDown(500);
    }, 3000);

    $('.cookie-container .button').on('click', function(e) {
        e.preventDefault();

        $('.cookie-container').slideUp(400);
    });

    //

    $('#go-pay-form').submit( function () {

        var th = $(this);
        $.ajax({
            type: "POST",
            url: "../send.php", //Change
            data: th.serialize()
        });

        var myPackeg = $('.my-pay-form').find('input[name=package]').val();
        var myEmail = $('.my-pay-form').find('input[name=email]').val();
        var myPhone = $('.my-pay-form').find('input[name=phone]').val();
        var myName = $('.my-pay-form').find('input[name=name]').val();

        localStorage.setItem('myKeyValue4', myPackeg);
        localStorage.setItem('myKeyValue3', myEmail);
        localStorage.setItem('myKeyValue2', myPhone);
        localStorage.setItem('myKeyValue1', myName);
    });

    var localValue1 = localStorage.getItem('myKeyValue1');
    var localValue2 = localStorage.getItem('myKeyValue2');
    var localValue3 = localStorage.getItem('myKeyValue3');
    var localValue4 = localStorage.getItem('myKeyValue4');

    $('#pay-ok').find('input[name=package]').val(localValue4);
    $('#pay-ok').find('input[name=email]').val(localValue3);
    $('#pay-ok').find('input[name=phone]').val(localValue2);
    $('#pay-ok').find('input[name=name]').val(localValue1);


    /*function setcookie(a,b,c) {
        if(c){
            var d = new Date();
                d.setTime(d.getTime()+c);
        }
        if(a && b) document.cookie = a+'='+ encodeURIComponent(b) +(c ? '; expires='+d.toUTCString() : '');else return false;
    }

    function getcookie(a) {
        var b = new RegExp(a+'=([^;]){1,}');
        var c = b.exec(document.cookie);
        if(c) c = c[0].split('=');else return false;return c[1] ? decodeURIComponent(c[1]) : false;
    }

    soundButton.click(function() {
        if(!sound) sound = 1;
        if (sound == 1) {
            setcookie("sound","1",30*3600*24*30*1000);
            document.getElementById("sound").style.backgroundPosition = "0px";
            audioLink.play();
        } else if (sound == 0) {
            setcookie("sound","0",30*3600*24*30*1000)
            document.getElementById("sound").style.backgroundPosition = "-40px";
        }
    });

    $(function () {
        if (getcookie("sound") == "1") audioLink.play()
    });*/



});
