<?php
session_start();

ignore_user_abort(true);

require_once('classes/AmoCrm.php');

/**
 * @param $data
 * @return string
 */
function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}

$name = clearData(($_POST['name']));
$email = clearData($_POST['email']);
$phone = clearData($_POST['phone']);
$question = clearData($_POST['question']);

$lid_name = clearData($_POST['lid-name']);
$teg = clearData($_POST['teg']);
$flowName = clearData($_POST['flowName']);
$package = clearData($_POST['package']);

$utmSource = clearData($_SESSION['utm_source']);
$utmMedium = clearData($_SESSION['utm_medium']);
$utmCampaign = clearData($_SESSION['utm_campaign']);
$utmTerm = clearData($_SESSION['utm_term']);
$utmContent = clearData($_SESSION['utm_content']);

$stage = '';
$thx_page = 'Location: thx.php';

if($lid_name == 'Забронировать место'){
    $stage = 22216462;
    $thx_page = 'Location: thx.php';
}elseif ($lid_name == 'Переход на оплату'){
    $stage = 22308157;
    $thx_page = 'Location: thx.php';
}elseif ($lid_name == 'Стать партнером'){
    $stage = 22216468;
    $thx_page = 'Location: lid-thx.php';
}elseif ($lid_name == 'Лидмагнит'){
    $stage = 22216465;
    $thx_page = 'Location: thx.php';
}elseif ($lid_name == 'Вопрос с сайта'){
    $stage = 22259938;
    $thx_page = 'Location: thx.php';
}elseif ($lid_name == 'Стать спонсором'){
    $stage = 22312999;
    $thx_page = 'Location: thx.php';
}

if(!empty($name) && !empty($phone)) {

    // Save user in crm

    $amoCrm = new AmoCrm([
        'USER_LOGIN' => 'levelup@smmstudio.com.ua',
        'USER_HASH'  => '68fc4408c840f2df05b63e75e222cd7d6d00fbca'
    ], 'levelupukraine');

    $lead = $amoCrm->storeLead($lid_name, $stage, $utmSource, $utmMedium, $utmCampaign, $utmTerm, $utmContent, $flowName, $teg, $package, $question);

    $leadId = $lead['response']['leads']['add'][0]['id'];

    $amoCrm->storeContact($name, $leadId, $email, $phone);


    header($thx_page);


} else {

    die('Data is empty!');

}