<section class="business-unique-playground">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Унікальний майданчик для бізнес-нетворкінгу</h2>
                <h3>Для людей, об’єднаних ідеєю новітніх трансформацій та створення win-win домовленостей між представниками
                    бізнесу, держави та соціуму
                </h3>
                <div class="items-wrapper">
                    <p class="item"><span class="number">01</span><span class="text">Єднання</span></p>
                    <p class="item"><span class="number">02</span><span class="text">Колаборація</span></p>
                    <p class="item"><span class="number">03</span><span class="text">Взаємопоміч</span></p>
                    <p class="item"><span class="number">04</span><span class="text">Довіра</span></p>
                    <p class="item"><span class="number">05</span><span class="text">Теорія шести рукостискань</span></p>
                    <p class="item"><span class="number">06</span><span class="text">Довгострокові відносини</span></p>
                    <p class="item"><span class="number">07</span><span class="text">Взаємовигідне співробітництво</span></p>
                </div>
                <h3>За час проведення заходу Level Up Ukraine ми зібрали більше 1000 корисних контактів, стали майданчиком
                    для сотень унікальних знайомств, вигідних угод та успішних інвестицій.
                </h3>
                <a href="#participation-packages" class="button scroll-to">Замовити квиток</a>
                <img src="img/playgarden-bg-pic.jpg" alt="" class="pic">
            </div>
        </div>
    </div>
</section>