<section class="program-more collapse" id="program-more">
    <div class="content">
        <div class="parts-wrapper">
            <div class="part part1">
                <h4>Час</h4>
            </div>
            <div class="part part2">
                <h4>
                    Трансформаційна сесія: індустрії
                </h4>
            </div>
            <div class="part part3">
                <h4>
                    Головна сцена
                </h4>
            </div>
            <div class="part part4">
                <h4>
                    Трансформаційна сесія: функції
                </h4>
            </div>
        </div>
        <h3 class="step-name">Відкриття</h3>
        <div class="step-time">
            <div class="time">9:00 – 9:30</div>
            <div class="step"><img src="img/coffe.svg" alt="">Реєстрація учасників і вітальна кава</div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot part-in">9:30 – 9:45</div>
            <div class="part part2 border-bot part-in"></div>
            <div class="part part3 border-bot part-in">
                <p>Відкриття: вітальне слово організаторів</p>
            </div>
            <div class="part part4 border-bot part-in"></div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 part-in">9:45 – 10:30</div>
            <div class="part part2 part-in"></div>
            <div class="part part3 part-in">
                <p><span>Key Speaker</span></p>
            </div>
            <div class="part part4"></div>
        </div>
        <h3 class="step-name">Сесія 1</h3>
        <div class="parts-wrapper">
            <div class="part part1">10:30 – 11:15</div>
            <div class="part part2">
                <h4 class="text-left">Впливовий HR-бренд в агробізнесі як частина трансформацій для аграрних компаній України
                    <br> <span>(панельна дискусія)</span>
                </h4>
            </div>
            <div class="part part3">
                <h4 class="text-left">Податкова та фінансова безпека майбутнього <br> <span>(панельна дискусія)</span></h4>
            </div>
            <div class="part part4">
                <h4 class="text-left">Майбутнє Fintech та банківської справи: перспектива дотримання стандартів</h4>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot "></div>
            <div class="part part2 border-bot ">
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Клеман Куссенс</span>, генеральний директор, Agro KMR</p>
                <p><span>Євген Дудка</span>, власник, генеральний директор, Група Компаній ТМ «Вілія» (уточнюється)</p>
                <p><span>Світлана Омельченко</span>, фінансовий директор, Agromino (уточнюється)</p>
                <img src="img/men-pic-1-2.svg" alt="">
                <p>Модератор: <br><span>Олександр Калюжний</span>, аналітик, УКАБ</p>
            </div>
            <div class="part part3">
                <img src="img/men-pic-2.svg" alt="">
                <!--<p><span>Віктор Ющенко</span>, Президент України 2005 – 2009 рр. (узгоджується)</p>-->
                <p><span>Рольф Барон фон Хохенхау</span>, Президент Європейської Асоціації платників податків</p>
                <p><span>Майкл Єгер</span>, Генеральний секретар Європейської Асоціації платників податків (узгоджується)</p>
                <p><span>Грігол Катамадзе</span>, Президент Асоціації платників податків України</p>
                <img src="img/men-pic-2-1.svg" alt="">
                <p>Модератор: <br><span>Наталія Ульянова</span>, партнер «Делойт» в Україні, віце-президент Асоціації
                    платників податків України
                </p>
            </div>
            <div class="part part4 border-bot">
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Віра Савва</span>, менеджер податково-юридичного департаменту «Делойт» в Україні</p>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 part-in">11:15 – 12:00</div>
            <div class="part part2 part-in">
                <h4 class="text-left">Небо, дорога, море: пошук інвестиційних можливостей в інфраструктурі </h4>
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Юрій Голік</span> Радник голови Дніпропетровської ОДА</p>
                <img src="img/men-pic-1-2.svg" alt="">
                <p>Модератор: <br><span>Дмитро Павленко</span>, партнер, податково-юридичний департамент «Делойт» в Україні
                </p>
            </div>
            <div class="part part3 part-in">

            </div>
            <div class="part part4 part-in">
                <h4 class="text-left">Управління брендом та репутацією</h4>
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Ольга Довганіч</span>, менеджер податково-юридичного департаменту «Делойт» в Україні</p>
            </div>
        </div>
        <h3 class="step-name">Сесія 2</h3>
        <div class="parts-wrapper">
            <div class="part part1">12:10 – 12:50</div>
            <div class="part part2">
                <h4 class="text-left">Експортний потенціал українських компаній <br><span>(панельна дискусія)</span></h4>
            </div>
            <div class="part part3">
                <h4 class="text-left">Майбутнє інвестицій в Україну</h4>
            </div>
            <div class="part part4">
                <h4 class="text-left">Практичний підхід до створення стратегії компанії</h4>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot"></div>
            <div class="part part2 border-bot">
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Грегорі Фишман</span>, директор, департамент розвитку бізнесу, «Делойт» в Україні</p>
                <p><span>Вікторія Чорновол</span>, партнер, податково-юридичний департамент, «Делойт» в Україні</p>
                <p><span>Пім Хаасдійк</span>, керуючий директор, Green Seed Group</p>
                <p><span>Світлана Дригуш</span>, інвестиційний директор, Horizon Capital</p>
            </div>
            <div class="part part3">
                <img src="img/men-pic-2.svg" alt="">
                <p><span>Мет Кеннеді</span>, Kennedy Foundation, Представник фонду "Black Rock" (узгоджується)</p>
                <p><span>Майкл Блейзер</span>, засновник Фонду Блейзера (узгоджується)</p>
                <p><span>Томаш Фіала</span>, CEO Dragon Capital (уточнюється)</p>
                <p><span>Волошина Олена</span>, голова представництва Міжнародної фінансової корпорації IFC в Україні (уточнюється)</p>
                <img src="img/men-pic-2-1.svg" alt="">
                <p>Модератор: <br><span>Гіві Топчишвілі</span>, Президент 9.8 Group</p>
            </div>
            <div class="part part4 border-bot">
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Андрій Котюжинський</span>, директор, департамент консалтингу «Делойт» в Україні</p>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 part-in">12:50 – 13:30</div>
            <div class="part part2 part-in">
                <h4 class="text-left">Гроші 3.0. Блокчейн як фінансовий інструмент</h4>
                <img src="img/men-pic-2.svg" alt="">
                <p><span>Сергій Калінін</span>, Директор з інвестицій, BlockBit Capital</p>
            </div>
            <div class="part part3 part-in">

            </div>
            <div class="part part4 part-in">
                <h4 class="text-left">Презентація результатів міжнародного дослідження «Тенденції у сфері управління персоналом – 2018»</h4>
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Людмила Корчак</span>, Консультант, департамент консалтингу «Делойт» в Україні</p>
            </div>
        </div>
        <div class="step-time">
            <div class="time">13:30 – 14:30</div>
            <div class="step"><img src="img/dinner.svg" alt="">Обід</div>
        </div>
        <h3 class="step-name">Сесія 3</h3>
        <div class="parts-wrapper">
            <div class="part part1">14:30 – 15:00</div>
            <div class="part part2">
                <h4 class="text-left">Трансформації на  фармацевтичному ринку</h4>
            </div>
            <div class="part part3">
                <h4 class="text-left">Українські Інноваційні лідери</h4>
            </div>
            <div class="part part4">
                <h4 class="text-left">Про інвестиційні проекти та залучення боргового фінансування</h4>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot"></div>
            <div class="part part2 border-bot">
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Євген Заїка</span>, генеральний директор «Фарма Старт»</p>
                <p><span>Констянтин Косяченко</span>, доктор фармацевтичних наук</p>
                <p><span>Володимир Костюк</span>, виконавчий директор, «Фармак»</p>
                <img src="img/men-pic-1-2.svg" alt="">
                <p>Модератор: <br><span>Наталія Бухалова</span>, генеральний директор, UMG</p>
            </div>
            <div class="part part3">
                <img src="img/men-pic-2.svg" alt="">
                <!--<p><span>Максим Нефьодов</span>, Перший заступник Міністра економічного розвитку і торгівлі, Виконуючий
                    обов'язки торгового представника України (узгоджується)
                <p><span>Андрій Пишний</span>, Голова Правління Ощадбанку (узгоджується)</p>
                <p><span>Мар’яна Каганяк</span>, Керівник Офісу з просування експорту (EPO), Радник Міністра економічного
                    розвитку і торгівлі (узгоджується)
                </p>-->
                <img src="img/men-pic-2-1.svg" alt="">
                <p>Модератор: <br><span>Денис Довгополий</span>, керуючий партнер компанії BayView Innovations</p>
            </div>
            <div class="part part4 border-bot">
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Ілля Сегеда</span>, старший менеджер «Делойт» в Україні</p>
                <p><span>Тарас Трохименко</span>, старший менеджер «Делойт» в Україні</p>
                <p><span>Андрій Путівський</span>, начальник відділу роботи з внутрішніми корпоративними клієнтами АТ
                    "Правексбанк"
                </p>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 border-bot part-in">15:00 – 15:30</div>
            <div class="part part2 border-bot part-in">
                <h4 class="text-left">Передові практики трансформації роздрібного бізнесу</h4>
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Сергій Бартощук</span>, голова наглядової ради Космо</p>
                <p><span>Андрій Логвин</span>, генеральний директор Kasta.ua (уточнюється)</p>
                <p><span>Игорь Хижняк</span>, генеральний директор Comfy (уточнюється)</p>
                <p><span>?</span>, генеральний директор мережі дитячих супермаркетів (уточнюється)</p>
                <img src="img/men-pic-1-2.svg" alt="">
                <p>Модератор: <br><span>Олександр Сопроненков</span>, директор «Делойт» в Україні, керівник індустріальної
                    групи роздріб, оптова торгівля і дистрибуція
                </p>
            </div>
            <div class="part part3 border-bot part-in">

            </div>
            <div class="part part4 border-bot part-in">
            </div>
        </div>
        <h3 class="step-name">Сесія 4</h3>
        <div class="parts-wrapper">
            <div class="part part1 border-bot">15:40 – 16:15</div>
            <div class="part part2 border-bot">
                <h4 class="text-left">Можливості та тренди в сфері зеленої енергетики</h4>
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Артур Огаджанян</span>, партнер, департамент корпоративних фінансів, індустуальний Energy&Utilities
                    лідер, «Делойт» в Україні
                </p>
            </div>
            <div class="part part3">
                <h4 class="text-left">Трансформаційні кейси українських компаній</h4>

                <img src="img/men-pic-2-1.svg" alt="">
                <p>Модератор: <br><span>Андрій Булах</span>, керуючий партнер «Делойт» в Україні</p>
            </div>
            <div class="part part4 border-bot">
                <h4 class="text-left">Досвід успішного залучення інвестора в поточних умовах</h4>
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Юрій Астахов</span>, директор, Драгон Капітал</p>
                <p><span>Андрій Гостик</span>, провідний банкір, Європейський банк реконструкції та розвитку </p>
                <p><span>?</span>, представник фонду прямих інвестицій (уточнюється)</p>
                <img src="img/men-pic-3.svg" alt="">
                <p>Модератор: <br><span>Дмитро Ануфрієв</span>, CFA, Партнер, «Делойт» в Україні, Корпоративні фінанси</p>
            </div>
        </div>
        <div class="parts-wrapper">
            <div class="part part1 part-in">16:15 – 17:00</div>
            <div class="part part2 part-in">
                <h4 class="text-left">Працюючи для приватного капіталу: чесно про успіхи і помилки</h4>
                <img src="img/men-pic-1.svg" alt="">
                <p><span>Андрій Серветник</span>, партнер, податково-юридичний департамент «Делойт» в Україні</p>
            </div>
            <div class="part part3 part-in">

            </div>
            <div class="part part4 part-in">
                <h4 class="text-left">Топ 10 юридичних факапів бізнесу, що стрімко зростає. Виконуємо роботу над помилками</h4>
                <img src="img/men-pic-3.svg" alt="">
                <p><span>Катерина Грабовик</span>, старший менеджер, податково-юридичний департамент «Делойт» в Україні</p>
            </div>
        </div>
        <div class="step-time">
            <div class="time">17:00 – 21:00</div>
            <div class="step"><img src="img/coctel.svg" alt="">Коктейль</div>
        </div>
    </div>
</section>