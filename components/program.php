<section class="program" id="program">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Ознайомтесь з програмою Level Up Ukraine 2018</h2>
                <p>Неможливо переоцінити значущість трансформацій і необхідність створення сприятливих умов для розвитку
                    бізнесу в Україні. Програма форуму максимально наповнена актуальними темами та майстер-класами від
                    впливових особистостостей.
                </p>
                <a href="#" class="button mob-btn" download="upload/program.pdf">Завантажити програму</a>
                <a href="#" class="desc-btn active-program" id="btn-program-open" data-toggle="collapse" data-target="#program-more">Ознайомитись з програмою</a>
                <img src="img/program-bg-pic.png" alt="" class="pic">
            </div>

        </div>
    </div>
</section>