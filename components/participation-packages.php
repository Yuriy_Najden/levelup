<section class="participation-packages" id="participation-packages">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Вартість участі у форумі</h2>
                <div class="items-wrapper">
                    <div class="item">
                        <div class="sale"></div>
                        <h3 class="pack-name">Basic</h3>
                        <p class="pack-price">2900 UAH</p>
                        <ul>
                            <li>Акредитація на Форумі</li>
                            <li>Доступ до головної сцени</li>
                            <li>Пакет учасника</li>
                            <li>Кава-брейки</li>
                            <li>Обід</li>
                            <li>Коктейль-джаз</li>
                            <li>Відправлення запису відео всіх доповідей форуму на пошту</li>
                        </ul>
                        <div class="buttons">
                            <a href="#" class="button" data-pakege="Basic" data-toggle="modal" data-target="#ticketModal">Замовити квиток</a>
                            <a href="#" class="booking" data-pakege="Basic" data-toggle="modal" data-target="#ticketModalBooking">Залишити заявку</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="sale">
                            <img src="img/sale.svg" alt="">
                            <p>
                                 При замовленні від 3 квитків – знижка 15%</p>
                        </div>
                        <h3 class="pack-name">Master-class</h3>
                        <p class="pack-price">4900 UAH</p>
                        <ul>
                            <li>Акредитація на Форумі</li>
                            <li>Доступ до головної сцени і до одного з потоків майстер класів на вибір</li>
                            <li>Пакет учасника</li>
                            <li>Кава-брейки</li>
                            <li>Обід</li>
                            <li>Коктейль-джаз</li>
                            <li>Відправка запису відео всіх потоків на пошту</li>
                        </ul>
                        <div class="buttons master-panel">
                            <a href="#" class="button" data-pakege="Master-class" data-toggle="modal" data-target="#ticketModal">Замовити квиток</a>
                            <a href="#" class="booking" data-pakege="Master-class" data-toggle="modal" data-target="#ticketModalBooking">Залишити заявку</a>
                        </div>
                    </div>
                    <div class="item">
                        <div class="sale">
                            <img src="img/sale.svg" alt="">
                            <p>
                                При замовленні від 3 квитків – знижка 15%</p>
                        </div>
                        <h3 class="pack-name">Master-class Plus</h3>
                        <p class="pack-price">5900 UAH</p>
                        <ul>
                            <li>Акредитація на Форумі</li>
                            <li>Доступ до головної сцени і до всіх потоків майстер класів на вибір</li>
                            <li>Пакет учасника</li>
                            <li>Кава-брейки</li>
                            <li>Обід</li>
                            <li>Коктейль-джаз</li>
                            <li>Відправка запису відео всіх потоків на пошту</li>
                        </ul>
                        <div class="buttons">
                            <a href="#" class="button" data-pakege="Master-class Plus" data-toggle="modal" data-target="#ticketModal">Замовити квиток</a>
                            <a href="#" class="booking" data-pakege="Master-class Plus" data-toggle="modal" data-target="#ticketModalBooking">Залишити заявку</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>