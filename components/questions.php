<section class="questions">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Маєте запитання?</h2>
                <p>Надішліть нам заповнену форму - і ми обов'язково з вами зв’яжемось</p>
                <form role="form" data-toggle="validator"  method="post" action="send.php">
                    <div class="form-col">
                        <div class="form-group inp-name has-feedback">
                            <label for="q-name">Ім’я</label>
                            <input type="text" class="form-control" name="name" id="q-name" placeholder="Введіть ваше ім’я" required>
                        </div>
                        <div class="form-group inp-phone has-feedback">
                            <label for="q-tel">Телефон</label>
                            <input type="tel" class="form-control" name="phone" id="q-tel" placeholder="Введіть ваш телефон" required>
                        </div>
                        <div class="form-group inp-email has-feedback">
                            <label for="q-email">Email</label>
                            <input type="email" class="form-control" name="email" id="q-email" placeholder="Введіть ваш email" required>
                        </div>
                    </div>
                    <div class="form-col">
                        <div class="form-group textarea-group inp-text has-feedback">
                            <label for="q-text">Питання</label>
                            <textarea id="q-text" name="question" class="form-control" placeholder="Введіть ваш питання" required></textarea>
                        </div>

                        <button type="submit" class="button">Відправити</button>
                    </div>
                    <input type="hidden" name="lid-name" value="Вопрос с сайта">
                </form>
                <img src="img/question-pic.png" alt="" class="pic">
            </div>

        </div>
    </div>
</section>