<section class="our-speakers" id="speakers">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Спікери форуму</h2>
                <div class="items-wrapper">
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-42.jpg" alt="">
                        </div>
                        <img src="img/ger-flag.svg" alt="" class="flag">
                        <h3>Рольф Барон фон Хохенхау</h3>
                        <p>Президент Європейської Асоціації платників податків</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-19.jpg" alt="">
                        </div>
                        <img src="img/usa-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Гіві Топчишвілі</h3>
                        <p>Президент 9.8 Group</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-9.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Андрій Булах</h3>
                        <p>Керуючий партнер компанії «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-2.jpg" alt="">
                        </div>
                        <img src="img/gr-flag.svg" alt="" class="flag">
                        <h3>Григол Катамадзе</h3>
                        <p>Президент Асоціації платників податків України</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-4.jpg" alt="">
                        </div>
                        <img src="img/fra-flag.svg" alt="" class="flag">
                        <h3>Клеман Куссенс</h3>
                        <p>Генеральний директор, Agro KMR</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-7.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Наталья Ульянова</h3>
                        <p>Партнер податково-юридичного департаменту «Делойт» в Україні, керівник практики комплексних
                            трансформацій бізнесу
                        </p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-43.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Сергій Калінін</h3>
                        <p>Директор з інвестицій, BlockBit Capital</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-8.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Заїка Євген</h3>
                        <p>Регіональний директор в країнах СНД Acino Group (Швейцарія) <br>
                            Генеральний директор ТОВ «Фарма Старт», Україна</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-30.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Наталія Бухалова</h3>
                        <p>Генеральний директор, UMG</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-32.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Юрій Астахов</h3>
                        <p>Директор, Драгон Капітал</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/nid-flag.svg" alt="" class="flag">
                        <h3>Пім Хаасдійк</h3>
                        <p>Керуючий директор, Green Seed Group</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Денис Довгополий</h3>
                        <p>Керуючий партнер компанії BayView Innovations </p>
                    </div>
                </div>
                <div class="items-wrapper more-speakers">
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-33.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Світлана Дригуш</h3>
                        <p>Інвестиційний директор, Horizon Capital</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-34.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Констянтин Магалецький </h3>
                        <p>Партнер, Horizon Capital </p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Володимир Косюк</h3>
                        <p>Виконавчий директор, "Фармак"</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-35.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Олександр Черінько</h3>
                        <p>"Керівник податково-юридичного департаменту, партнер «Делойт» в
                            Україні"</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-27.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Вікторія Чорновол</h3>
                        <p>Партнер, податково-юридичний департамент «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-26.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Артур Огаджанян</h3>
                        <p>Партнер, департамент корпоративних фінансів «Делойт» в Україні, керівник групи з оцінки і
                            фінансового моделювання; сертифікований оцінювач бізнесу, нематеріальних активів та основних
                            засобів.
                        </p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-28.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Грегорі Фішман</h3>
                        <p>Директор, департамент розвитку бізнесу «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-11.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Андрій Серветник</h3>
                        <p>Партнер, податково-юридичний департамент «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-36.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Дмитро Ануфрієв</h3>
                        <p>CFA, Партнер, департамент корпоративних фінансів  «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-37.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Андрій Котюжинський </h3>
                        <p>Директор, департамент консалтингу «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Людмила Корчак</h3>
                        <p>Консультант, департамент консалтингу «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-24.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Олександр Калюжний</h3>
                        <p>Аналітик, УКАБ</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-10.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Костянтин Косяченко</h3>
                        <p>Доктор фармацевтичних наук, професор кафедри організації і економіки фармації, Національна
                            медична академія післядипломної освіти імені П. Л. Шупика</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Андрій Гостик</h3>
                        <p>провідний банкір, Європейський банк реконструкції та розвитку </p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Андрій Путівський</h3>
                        <p>"начальник відділу роботи з внутрішніми корпоративними клієнтами,ПАТ ""Правексбанк"""</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-31.jpg" alt="">
                        </div>
                        <img src="img/ch-flag.svg" alt="" class="flag">
                        <h3>Юрій Голік</h3>
                        <p>Радник голови Дніпропетровської ОДА</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-38.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Ілля Сегеда</h3>
                        <p>старший менеджер «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-39.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Тарас Трохименко</h3>
                        <p>старший менеджер «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-40.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Катерина Грабовик </h3>
                        <p>старший менеджер, податково-юридичний департамент «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-41.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Наталія Кроник</h3>
                        <p>Менеджер податково-юридичний департамент «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-5.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Віра Савва</h3>
                        <p>Менеджер податково-юридичного департаменту «Делойт» в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-6.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <h3>Ольга Довганіч</h3>
                        <p>Менеджер податково-юридичного департаменту «Делойт» в Україні</p>
                    </div>
                    <!--<div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-12.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Віктор Ющенко</h3>
                        <p>Президент України 2005 – 2009 рр.</p>
                    </div>-->
                    <div class="item"">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-13.jpg" alt="">
                        </div>
                        <img src="img/ger-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Майкл Єгер</h3>
                        <p>Генеральний секретар Європейської Асоціації платників податків</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-14.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Євген Дудка</h3>
                        <p>Власник, генеральний директор, Група Компаній ТМ «Вілія» </p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-15.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Світлана Омельченко</h3>
                        <p>Фінансовий директор, Agromino</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-0.jpg" alt="">
                        </div>
                        <img src="img/usa-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Мет Кеннеді</h3>
                        <p>Kennedy Foundation</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-16.jpg" alt="">
                        </div>
                        <img src="img/usa-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Майкл Блейзер</h3>
                        <p>Засновник Фонду Блейзера</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-17.jpg" alt="">
                        </div>
                        <img src="img/ch-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Томаш Фіала</h3>
                        <p>CEO Dragon Capital</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-18.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Олена Волошина</h3>
                        <p>Голова представництва Міжнародної фінансової корпорації IFC в Україні</p>
                    </div>
                    <div class="item">
                        <div class="speak-photo">
                            <img src="pic/spick-pic-22.jpg" alt="">
                        </div>
                        <img src="img/ukr-flag.svg" alt="" class="flag">
                        <p class="none">(не підтверджено)</p>
                        <h3>Мар’яна Каганяк</h3>
                        <p>Керівник Офісу з просування експорту (EPO), Радник Міністра економічного розвитку і торгівлі</p>
                    </div>
                </div>
                <a href="#" class="open-more-speakers" id="open-more-speakers">Всі спікери</a>
            </div>

        </div>
    </div>
</section>