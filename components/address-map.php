<section class="address-map">
    <div class="address">
        <h2 class="block-title">Час і місце зустрічі</h2>
        <div class="item">
            <img src="img/datapiker.svg" alt="">
            <p>28 листопада 2018 <br> 9:00</p>
        </div>
        <div class="item">
            <img src="img/place-icon.svg" alt="">
            <p>Київ, Набережно-Хрещатицька <br> 1 Fairmont Grand Hotel Kyiv</p>
        </div>
    </div>
    <div class="map" id="map"></div>
</section>