<section class="main-features">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">6 головних можливостей учасників Level Up Ukraine 2018</h2>
                <div class="items-wrapper">
                    <div class="item">
                        <p class="border-text">01</p>
                        <p class="text">Відвідати прогресивні майстер-класи на тему трансформації бізнесу</p>
                    </div>
                    <div class="item">
                        <p class="border-text">02</p>
                        <p class="text">Підготуватися до змін, що чекають на бізнес в українському і міжнародному податковому
                            полі
                        </p>
                    </div>
                    <div class="item">
                        <p class="border-text">03</p>
                        <p class="text">Дізнатися про ринок майбутнього: яким він буде і до чого готуватися вже сьогодні</p>
                    </div>
                    <div class="item">
                        <p class="border-text">04</p>
                        <p class="text">Почути дієві поради світових лідерів на арені бізнес-трансформацій</p>
                    </div>
                    <div class="item">
                        <p class="border-text">05</p>
                        <p class="text">Обговорити реальні трансформаційні приклади та міжнародні тенденції</p>
                    </div>
                    <div class="item">
                        <p class="border-text">06</p>
                        <p class="text">Познайомитись із впливовими власниками бізнесу та інвесторами</p>
                    </div>
                </div>
                <a href="#participation-packages" class="button scroll-to">Замовити квиток</a>
            </div>
        </div>
    </div>
</section>