<section class="jazz">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Бізнес, трансформації, інвестиції та джаз</h2>
                <img src="img/jazz.png" alt="" class="pic">
                <p>Наприкінці насиченої програми форуму, у кращих традиціях всеохоплюючого нетворкінгу, ми запрошуємо учасників
                    на джазову вечірку за участі талановитого джаз-бенду.
                </p>
            </div>
        </div>
    </div>
</section>