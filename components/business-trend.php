<section class="business-trend">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Трансформація бізнесу</h2>
                <h3>Сьогодні перед бізнесом постає безліч викликів: поява нових бізнес моделей, дигіталізація, глобалізація
                    ринків збуту, зростання конкуренції за персонал та капітал, посилення вимог регуляторів, підвищення
                    прозорості фінансових транзакцій. За прогнозами експертів, тиск цих та інших факторів призведе до того,
                    що середній термін життя компанії скоротиться з 60 років до 15 років. Виживуть лише ті, хто змінюється
                    відповідно до вимог часу. Тому цьогорічний форум присвячений саме темі “трансформація бізнесу”.
                </h3>
                <div class="items-wrapper">
                    <div class="item">
                        <img src="img/business-life-pic-1.svg" alt="">
                        <div class="text">
                            <h4>Корисно:</h4>
                            <p>провідним компаніям та бізнесу, що розвивається</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/business-life-pic-2.svg" alt="">
                        <div class="text">
                            <h4>Актуально:</h4>
                            <p>інвестору  та власнику бізнесу</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="img/business-life-pic-3.svg" alt="">
                        <div class="text">
                            <h4>Цікаво:</h4>
                            <p>топ-менеджеру та посадовій особі</p>
                        </div>
                    </div>
                </div>
                <div class="buttons-wrapper">
                    <a href="#participation-packages" class="button scroll-to">Подати заявку</a>
                    <a href="#program" class="button scroll-to">Програма форуму</a>
                </div>

            </div>
        </div>
    </div>
</section>