<section class="forum-numbers">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Цього року на форумі Level Up Ukraine 2018</h2>
                <div class="items-wrapper">
                    <div class="item">
                        <p class="border-text">25</p>
                        <p class="text">спікерів зі світовим досвідом</p>
                    </div>
                    <div class="item">
                        <p class="border-text">150</p>
                        <p class="text">власників різноманітних бізнес-підприємств</p>
                    </div>
                    <div class="item">
                        <p class="border-text">15</p>
                        <p class="text">відомих міжнародних інвесторів</p>
                    </div>
                    <div class="item">
                        <p class="border-text">3</p>
                        <p class="text">секції виступів і панельних дискусій </p>
                    </div>
                    <div class="item">
                        <p class="border-text">16</p>
                        <p class="text">майстер-класів і спеціалізованих дискусій</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>