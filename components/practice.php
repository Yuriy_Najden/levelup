<section class="practice">
    <div class="container">
        <div class="row">
            <div class="content col-md-12">
                <h2 class="block-title">Практика від Level UP Ukraine</h2>
                <h3>Це експерти з трансформацій, які беруть  активну участь у впровадженні якісних змін на рівні бізнесу
                    та компаній.
                </h3>
                <ul>
                    <li>
                        <p class="number">01</p>
                        <p class="text">Лідери, які трансформують Державу та Націю</p>
                    </li>
                    <li>
                        <p class="number">02</p>
                        <p class="text">Бізнесмени, які трансформують ринок та піднімають  рівень якості послуг</p>
                    </li>
                    <li>
                        <p class="number">03</p>
                        <p class="text">Законотворці та політики, які трансформують напрями та правила гри</p>
                    </li>
                    <li>
                        <p class="number">04</p>
                        <p class="text">Правознавці, які трансформують підходи та практики</p>
                    </li>
                </ul>
                <h3>Ми віримо, що дискусія без практики – це подорож із зав’язаними очима</h3>
                <a href="#participation-packages" class="button scroll-to">Замовити квиток</a>
                <img src="img/practic-bg-pic.png" alt="" class="pic">
            </div>

        </div>
    </div>
</section>