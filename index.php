<?php

session_start();

if (!empty($_GET['utm_source'])) {

    $_SESSION['utm_source'] = $_GET['utm_source'];
}

if (!empty($_GET['utm_medium'])) {

    $_SESSION['utm_medium'] = $_GET['utm_medium'];
}

if (!empty($_GET['utm_campaign'])) {

    $_SESSION['utm_campaign'] = $_GET['utm_campaign'];
}

if (!empty($_GET['utm_term'])) {

    $_SESSION['utm_term'] = $_GET['utm_term'];
}

if (!empty($_GET['utm_content'])) {

    $_SESSION['utm_content'] = $_GET['utm_content'];
}
?>

<!DOCTYPE html>
<html lang="ua">
  <head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="/img/og.png" />

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

      <meta name="msapplication-TileColor" content="#000000">

      <meta name="theme-color" content="#000000">

    <title>LevelUp</title>

      <link rel="stylesheet" href="css/animate.css">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/jquery.fancybox.css">
      <link type="text/css" rel="stylesheet" href="css/style.css"/>

      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
              new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
              j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-W44ZTV8');</script>
      <!-- End Google Tag Manager -->

  </head>
  <body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W44ZTV8"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
    <div class="wrapper">
        <div class="loader" id="loader"></div>

        <header>
            <div class="fix-menu">
                <div class="cookie-container">
                    <div class="container">
                        <div class="row">
                            <div class="content col-md-12">
                                <p>Даний сайт використовує cookie-файли з метою забезпечення більш оперативного та індивідуального
                                    обслуговування. Продовжуючи користуватися даним сайтом, ви погоджуєтеся з використанням нами
                                    cookie-файлів. Детальна інформація про cookie-файли, які використовуються на сайті, а також
                                    способи їх видалення чи блокування доступна в розділі <br> <a href="https://www2.deloitte.com/ua/uk/footerlinks1/cookies.html" target="_blank">«Умови використання cookie-файлів»</a> .
                                </p>
                                <a href="#" class="button">
                                    <svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M13 1H1V13H13V8" stroke="#0076A8" stroke-width="2"/>
                                        <path d="M3 5L7.5 9.5L15 2" stroke="#0076A8" stroke-width="2"/>
                                    </svg>
                                    Прийняти та закрити
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="logo-phone">
                    <div class="container">
                        <div class="row">
                            <div class="content col-md-12">
                                <div class="logo-wrapper">
                                    <img src="img/logo.svg" alt="" class="logo">
                                    <div class="line"></div>
                                    <p>Level Up Ukraine 2018</p>
                                </div>
                                <div class="phone-wrapper">
                                    <a href="tel:08001111111" class="phone">0 800 211 213</a>
                                    <a href="#" class="callback">Замовити дзвінок</a>
                                </div>
                                <a href="#" class="mob-menu-btn" id="menu-btn">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </a>
                                <nav class="mob-menu" id="mob-menu">
                                    <ul>
                                        <li><a href="#speakers" class="scroll-to">Спікери</a></li>
                                        <li><a href="#program" class="scroll-to">Програма</a></li>
                                        <li><a href="#participation-packages" class="scroll-to">Пакети участі</a></li>
                                    </ul>
                                    <div class="fix-content">
                                        <div class="lang">
                                            <a href="#" class="active-lang">УКР</a>
                                            <a href="http://levelupukraine.com/en/">ENG</a>
                                        </div>
                                        <div class="mob-phone-wrapper">
                                            <div class="inner">
                                                <img src="img/phone-icon.svg" alt="">
                                                <div>
                                                    <a href="tel:08001111111" class="phone">0 800 111 11 11</a>
                                                    <a href="#" class="callback">Замовити дзвінок</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <nav>
                    <div class="container">
                        <div class="row">
                            <div class="content col-md-12">
                                <div class="menu">
                                    <ul>
                                        <li><a href="#speakers" class="scroll-to">Спікери</a></li>
                                        <li><a href="#program" class="scroll-to">Програма</a></li>
                                        <li><a href="#participation-packages" class="scroll-to">Пакети участі</a></li>
                                    </ul>
                                </div>
                                <div class="lang">
                                    <a href="#" class="active-lang">Українською</a>
                                    <div class="line"></div>
                                    <a href="http://levelupukraine.com/en/">English</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>

            <div class="header-content">
                <div class="container">
                    <div class="row">
                        <div class="content col-md-12">
                            <img src="img/header-mob-pic.png" alt="" class="mob-pic">
                            <h1>Level Up Ukraine 2018</h1>
                            <p><span>Унікальний майданчик для бізнес-нетворкінгу <br>
                                Fairmont Grand Hotel Kyiv <br></span>
                                28 листопада 2018
                            </p>
                            <a href="#participation-packages" class="button scroll-to">Замовити квиток</a>
                            <a href="#program" class="program-btn scroll-to">Програма форуму</a>
                            <img src="img/header-pic.png" alt="" class="pic">
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <?php include_once 'components/business-trend.php'?>

        <?php include_once 'components/main-topics.php'?>

        <?php include_once 'components/practice.php'?>

        <?php include_once 'components/forum-numbers.php'?>

        <?php include_once 'components/our-speakers.php'?>

        <?php include_once 'components/program.php'?>

        <?php include_once 'components/program-more.php'?>

        <?php include_once 'components/video-join.php'?>

        <?php include_once 'components/participation-packages.php'?>

        <?php include_once 'components/what-do-experts-say.php'?>
        
        <?php include_once 'components/business-unique-playground.php'?>

        <?php include_once 'components/jazz.php'?>

        <?php include_once 'components/our-experience.php'?>

        <?php include_once 'components/gallery.php'?>

        <?php include_once 'components/organizer.php'?>

        <?php include_once 'components/sponsors.php'?>

        <?php include_once 'components/main-fatures.php'?>

        <?php include_once 'components/address-map.php'?>

        <?php include_once 'components/questions.php'?>

        <?php include_once 'components/pre-footer.php'?>
        
        <?php include_once 'components/footer.php'?>
    </div>

    <?php include ('components/popup.php');?>


    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!--[if lt IE 7]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyACxR3qD-AgGQmQeiYC4YMwHdoKTZj5TT0"></script> <!--&callback=initMap -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


  <!--<script src="js/jquery-3.2.1.min.js"></script>-->

    <script src="js/bootstrap.min.js"></script>

    <script src="js/validator.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/youtube.js"></script>
    <script src="js/map.js"></script>

    <script src="js/js.js"></script>
    
  </body>
</html>
